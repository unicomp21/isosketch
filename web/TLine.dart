part of OmniDraw;

class TLine {
  List<vmath.Vector2> _points = new List<vmath.Vector2>();
  void add(vmath.Vector2 v) {
    assert(_points.length < 2);
    _points.add(v);
  }
  int get length { return _points.length; }
  vmath.Vector2 draw(CanvasRenderingContext2D ctx, vmath.Vector2 last) {
    assert(_points.length == 2);
    if(last != _points[0])
      ctx.moveTo(_points[0].x, _points[0].y);
    ctx.lineTo(_points[1].x, _points[1].y);
    return _points[1];
  }
}
