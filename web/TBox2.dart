part of OmniDraw;

class TBox2 {
  num _x;
  num _y;
  num _width;
  num _height;
  TBox2(this._x, this._y, this._width, this._height);
  num get x { return _x; }
  num get y { return _y; }
  num get x2 { return _x + _width; }
  num get y2 { return _y + _height; }
  num get width { return _width; }
  num get height { return _height; }
  bool Contains(vmath.Vector2 pt) {
    return (pt.x >= x) && (pt.x < x2) &&
        (pt.y >= y) && (pt.y < y2);
  }
  void Merge(vmath.Vector2 pt) {
    if(pt.x < x) this._x = x;
    if(pt.x > x2) this._x = x;
    if(pt.y < y) this._y = y;
    if(pt.y > y2) this._y = y;
  }
  Render(CanvasRenderingContext2D renderContext, String color) {
    renderContext.beginPath();
    renderContext.lineWidth = 1;
    renderContext.strokeStyle = color;
    renderContext.rect(0, 0, 0, 0);
    renderContext.stroke();
    renderContext.rect(_x, _y, _width, _height);
    renderContext.closePath();
    renderContext.stroke();
  }
}
