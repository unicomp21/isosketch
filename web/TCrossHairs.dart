part of OmniDraw;

class TCrossHairs {
  Map<int, TCrossHair> _crossHairs = new HashMap<int, TCrossHair>();
  int _crossHairId = 1;
  Add(TCrossHair crossHair) {
    _crossHairs[_crossHairId] = crossHair;
    _crossHairId++;
  }
  TCrossHair Get(int id) { return _crossHairs[id]; }
  Remove(int id) {
    return _crossHairs.remove(id);
  }
  int Select(MouseEvent e) {
    for(int crossHairId in _crossHairs.keys) {
      if(_crossHairs[crossHairId].Hit(e)) {
        return crossHairId;
      }
    }
    return 0;
  }
  Map<int, TCrossHair> get items { return _crossHairs; }
}
