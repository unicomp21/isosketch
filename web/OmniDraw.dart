library OmniDraw;

import 'dart:html';
import 'dart:math' as math;
import 'dart:collection';
import 'package:vector_math/vector_math.dart' as vmath;
part 'TCrossHair.dart';
part 'TCrossHairs.dart';
part 'TBox2.dart';
part 'TGui2D.dart';
part 'TSurfaceMarch2d.dart';
part 'TLine.dart';
part 'THeatMap.dart';
part 'TField2D.dart';

/////////////
void main() {
  new TGui2D(query("#canvas"));
}
