part of OmniDraw;

class TSurfaceMarch2d {
  TCrossHairs _crossHairs = null;
  double _march_step = 0.0;
  double _surface_temperature = 0.0;
  TSurfaceMarch2d(this._crossHairs, this._march_step, this._surface_temperature) {
    _MarchSurface();
  }
  void _MarchSurface() {
    for(TCrossHair crossHair in _crossHairs.items.values) {
      vmath.Vector2 v = _SeekBoundary(crossHair.pt);
      _MarchBoundary(v);
    }
  }
  vmath.Vector2 _SeekBoundary(vmath.Vector2 v) {
    v = (v / _march_step.toDouble());
    v.x = v.x.floorToDouble();
    v.y = v.y.floorToDouble();
    v.scale(_march_step);
    while(!_IsBoundaryRect(v))
      v.x += _march_step;
    //print(v.toString());
    return v;
  }
  bool _IsHot(vmath.Vector2 v) {
    return (_Temperature(v) > _surface_temperature);
  }
  List<TLine> _surface = new List<TLine>();
  List<TLine> get Surface { return _surface; }
  bool _IsBoundaryRect(vmath.Vector2 v) {
    int tri_count = 0;
    
    List<vmath.Vector2> tri1 = new List<vmath.Vector2>();
    tri1.add(v + new vmath.Vector2(0 * _march_step, 0 * _march_step));
    tri1.add(v + new vmath.Vector2(1 * _march_step, 0 * _march_step));
    tri1.add(v + new vmath.Vector2(1 * _march_step, 1 * _march_step));
    if(_IsBoundaryTri(tri1)) tri_count++;
    
    List<vmath.Vector2> tri2 = new List<vmath.Vector2>();
    tri2.add(v + new vmath.Vector2(0 * _march_step, 0 * _march_step));
    tri2.add(v + new vmath.Vector2(1 * _march_step, 1 * _march_step));
    tri2.add(v + new vmath.Vector2(0 * _march_step, 1 * _march_step));
    if(_IsBoundaryTri(tri2)) tri_count++;
    
    return (tri_count > 0);
  }
  bool _IsBoundaryTri(List<vmath.Vector2> tri) {
    assert(tri.length == 3);
    List<vmath.Vector2> hots = new List<vmath.Vector2>();
    List<vmath.Vector2> colds = new List<vmath.Vector2>();
    for(vmath.Vector2 v in tri) {
      if(this._IsHot(v))
        hots.add(v);
      else
        colds.add(v);
    }
    TLine line = new TLine();
    for(vmath.Vector2 vhot in hots)
      for(vmath.Vector2 vcold in colds) {
        line.add(_TemperatureSurface(vhot, vcold));
        if(line.length == 2) {
          _surface.add(line);
          line = new TLine();
        }
      }
    return((hots.length != 0) && (colds.length != 0));
  }
  vmath.Vector2 _TemperatureSurface(vmath.Vector2 v0, vmath.Vector2 v1) {
    num t0 = _Temperature(v0);
    num t1 = _Temperature(v1);
    vmath.Vector2 offset = v1 - v0;
    num k = (_surface_temperature - t0) / (t1 - t0);
    return (v0 + offset.scale(k.toDouble()));
  }
  static int _VectorKey(vmath.Vector2 v) {
    return (v.x.toInt()) + (v.y.toInt() << 24);
  }
  _MarchBoundary(vmath.Vector2 v) {
    int key = _VectorKey(v);
    if(!_boundary_cache.containsKey(key)) {
      if(_IsBoundaryRect(v)) {
        _boundary_cache[key] = v;
        for(int x = -1; x < 2; x++)
          for(int y = -1; y < 2; y++) {
            vmath.Vector2 sample =
                v + new vmath.Vector2(x * _march_step, y * _march_step);
            _MarchBoundary(sample);
          }
      }
    }
  }
  Map<int, vmath.Vector2> _boundary_cache = new HashMap<int, vmath.Vector2>();
  double _Temperature(vmath.Vector2 pt) {
    return _TemperatureSlowNAccurate(pt);
  }
  Map<int, double> _temperature_cache = new HashMap<int, double>();
  double _TemperatureSlowNAccurate(vmath.Vector2 pt) {
    int key = _VectorKey(pt);
    if(_temperature_cache.containsKey(key)) {
      return _temperature_cache[key];
    } else {
      double sum = 0.0;
      for(TCrossHair crossHair in _crossHairs.items.values) {
        vmath.Vector2 offset = crossHair.pt.clone().sub(pt);
        sum += (1.0 / (1.0 + offset.length2));
      }
      _temperature_cache[key] = sum;
      return sum;
    }
  }
}
