part of OmniDraw;

class TField2D {
  Map<int, double> _field = new HashMap<int, double>();
  TField2D() { }
  _MakeKey(int x, int y) { return ((1 << 21) + (y << 20)) + x; }
  void SetValue(int x, int y, double val) { _field[_MakeKey(x, y)] = val; }
  double GetValue(int x, int y) {
    int key = _MakeKey(x, y);
    if(_field.containsKey(key)) {
      return _field[key];
    } else {
      return 0.0;
    }
  }
  double GetValueLerp(double x, double y) {
    int x_floor = x.floor().toInt();
    int y_floor = y.floor().toInt();
    double frac_x = x - x_floor;
    double frac_y = y - y_floor;
    
    double f_00 = GetValue(x_floor, y_floor);
    double f_10 = GetValue(x_floor + 1, y_floor);
    double f_11 = GetValue(x_floor + 1, y_floor + 1);
    double f_01 = GetValue(x_floor, y_floor + 1);
    
    double lerp_x0 = lerp(frac_x, f_00, f_10);
    double lerp_x1 = lerp(frac_x, f_01, f_11);
    double lerp_y = lerp(frac_y, lerp_x0, lerp_x1);
    
    return lerp_y;
  }
  double GetValueVec2(vmath.Vector2 v) { return GetValueLerp(v.x, v.y); }
  void AddPointField(vmath.Vector2 v, double radius, double scale) {
    int floor_x = v.x.floor();
    int floor_y = v.y.floor();
    for(double x = -radius; x <= radius; x++) {
      for(double y = -radius; y <= radius; y++) {
        vmath.Vector2 check_radius = new vmath.Vector2(x, y);
        if(check_radius.length <= radius) {
          vmath.Vector2 offset = new vmath.Vector2(floor_x + x, floor_y + y);
          offset.sub(v);
          double add_level = (1.0 / offset.length2) * scale;
          double fetch_level = GetValue(
              (floor_x + x).toInt(), (floor_y + y).toInt());
          SetValue(
              (floor_x + x).toInt(), (floor_y + y).toInt(), 
              add_level + fetch_level);
        }
      }
    }
  }
}
