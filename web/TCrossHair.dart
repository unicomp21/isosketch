part of OmniDraw;

class TCrossHair {
  final double _voxel_radius = 16.0;
  TGui2D _gui2D;
  TCrossHair(this._gui2D, this._pt);
  num _draw_radius = 6;
  num get radius { return _draw_radius; }
  vmath.Vector2 _pt;
  vmath.Vector2 get pt { return _pt; }
  CanvasRenderingContext2D get _renderContext { return _gui2D._renderContext; }
  Render(String strokeStyle) {
    vmath.Vector2 view_pt = _gui2D.toViewCoord(_pt);
    _renderContext.beginPath();
    _renderContext.lineWidth = 1;
    _renderContext.strokeStyle = strokeStyle;
    _renderContext.moveTo(view_pt.x, view_pt.y);
    _renderContext.arc(view_pt.x, view_pt.y, _draw_radius, 0, 2 * math.PI, false);
    _renderContext.moveTo(view_pt.x - _draw_radius, view_pt.y);
    _renderContext.lineTo(view_pt.x + _draw_radius, view_pt.y);
    _renderContext.moveTo(view_pt.x, view_pt.y - _draw_radius);
    _renderContext.lineTo(view_pt.x, view_pt.y + _draw_radius);
    _renderContext.closePath();
    _renderContext.stroke();
  }
  bool Hit(MouseEvent e) {
    vmath.Vector2 test = _gui2D.toViewCoord(
        new vmath.Vector2(e.client.x.toDouble(), e.client.y.toDouble()));
    num dist = (_pt - test).length;
    bool check = dist <= _draw_radius;
    return check;
  }
  void MoveTo(vmath.Vector2 pt) {
    this._pt = pt;
  }
}

