part of OmniDraw;

double lerp(double scale, double x, double y) {
  assert(scale <= 1.0);
  return (x * (1.0 - scale)) + (y * scale);
}

class THeatMap {
  final int _width = 256;
  List<double> _heat_field = null;
  THeatMap() {
    _heat_field = new List<double>(_width * _width);
    for(int x = 0; x < _width; x++) {
      for(int y = 0; y < _width; y++) {
        double temp = 1.0 / ((x * x) + (y * y));
        int index = (y << 8) + x;
        _heat_field[index] = temp;
      }
    }
  }
  double Temp(int x, int y) {
    int x_abs = x.abs();
    int y_abs = y.abs();
    int index = (y_abs << 8) + x_abs;
    return ((x_abs < (_width - 1)) && (y_abs < (_width - 1))) ? _heat_field[index] : 0.0;
  }
  double TempLerp(double x, double y) {
    int int_x = x.floor().toInt();
    int int_y = y.floor().toInt();
    double frac_x = x - int_x;
    double frac_y = y - int_y;

    double temp_00 = Temp(int_x, int_y);
    double temp_10 = Temp(int_x + 1, int_y);
    double temp_11 = Temp(int_x + 1, int_y + 1);
    double temp_01 = Temp(int_x, int_y + 1);
    
    double lerp_x0 = lerp(frac_x, temp_00, temp_10);
    double lerp_x1 = lerp(frac_x, temp_01, temp_11);
    double lerp_y = lerp(frac_y, lerp_x0, lerp_x1);
    
    return lerp_y;
  }
}
