part of OmniDraw;

class TGui2D {
  CanvasElement _canvas = null;
  CanvasRenderingContext2D _renderContext = null;
  TCrossHairs _crossHairs = new TCrossHairs();
  vmath.Vector2 _view_offset = new vmath.Vector2(0.0, 0.0);
  THeatMap _heatMap = new THeatMap();
  TGui2D(CanvasElement this._canvas) {
    this._renderContext = _canvas.context2D;
    _sizing();
    _registerEvents();
  }
  void _sizing() {
    _canvas.width = window.innerWidth;
    _canvas.height = window.innerHeight;
    _update();
  }
  vmath.Vector2 toViewCoord(vmath.Vector2 pt) {
    vmath.Vector2 viewCoord = new vmath.Vector2(_view_offset.x + pt.x, _view_offset.y + pt.y);
    return viewCoord;
  }
  vmath.Vector2 fromViewCoord(vmath.Vector2 view_pt) {
    vmath.Vector2 pt = new vmath.Vector2(view_pt.x - _view_offset.x, view_pt.y - _view_offset.y);
    return pt;
  }
  void _registerEvents() {
    _canvas.onTouchMove.listen((e) { _touchMove(e); });
    _canvas.onMouseMove.listen((e) { _mouseMove(e); });

    _canvas.onTouchStart.listen((e) { _mouseDown(e); });
    _canvas.onMouseDown.listen((e) { _mouseDown(e); });

    window.onResize.listen((e) { _sizing(); });
    window.onDeviceOrientation.listen((e) { _sizing(); });
  }
  void _touchMove(TouchEvent e) {
    //print("touchMove: ${e.touches}");
    vmath.Vector2 pt = new vmath.Vector2(e.page.x - _canvas.offsetLeft,
        e.page.y - _canvas.offsetTop);
    _pointerMove(pt);
  }
  TBox2 _trash = new TBox2(0, 0, 50, 50);
  void _mouseMove(MouseEvent e) {
    //print("mouseMove: ${e.x}, ${e.y}");
    vmath.Vector2 pt = new vmath.Vector2(e.client.x.toDouble(), e.client.y.toDouble());
    _pointerMove(pt);
  }
  void _pointerMove(vmath.Vector2 pt) {
    if(_selectedCrossHairId != 0) {
      _crossHairs.Get(_selectedCrossHairId).MoveTo(fromViewCoord(pt));
      if(_trash.Contains(pt)) {
        _crossHairs.Remove(_selectedCrossHairId);
        _selectedCrossHairId = 0;
      }
      _update();    
    }
  }
  TField2D _field2D = new TField2D();
  void _mouseDown(MouseEvent e) {
    //print("mouseDown: ${e.x}, ${e.y}");
    if(_selectedCrossHairId != 0) {
      _selectedCrossHairId = 0;
    }
    else {
      int id = _crossHairs.Select(e);
      if(0 == id) {
      _crossHairs.Add(new TCrossHair(
          this, new vmath.Vector2(e.client.x.toDouble(), e.client.y.toDouble())));
      } else {
        _selectedCrossHairId = id;
      }
    }
    _update();
  }
  int _selectedCrossHairId = 0;
  _clear() {
    _renderContext.clearRect(0, 0, _canvas.width, _canvas.height);
  }
  void _draw_crosshairs() {
    for(int crossHairId in _crossHairs.items.keys) {
      if(_selectedCrossHairId != crossHairId) {
        _crossHairs.Get(crossHairId).Render('green');
      } else {
        _crossHairs.Get(_selectedCrossHairId).Render('yellow');
      }
    }
    _trash.Render(_renderContext, 'red');
  }
  void _draw_surface() {
    TSurfaceMarch2d surfaceMarch2d = 
        new TSurfaceMarch2d(_crossHairs, 21.0, 0.003);
    List<TLine> surface_plot = surfaceMarch2d.Surface;
    _renderContext.fillStyle = 'yellow';
    _renderContext.strokeStyle = 'yellow';
    vmath.Vector2 last = new vmath.Vector2(0.0, 0.0);
    for(TLine line in surface_plot)
      last = line.draw(_renderContext, last);
    _renderContext.stroke();
  }
  void _update() {
    window.requestAnimationFrame(_animation_callback);
  }
  void _animation_callback(num timer) {
    _clear();
    _draw_crosshairs();
    _draw_surface();
  }
}
